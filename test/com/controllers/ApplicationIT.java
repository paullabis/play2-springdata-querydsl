package com.controllers;

import components.play.Global;
import controllers.Application;
import models.Person;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import play.mvc.Result;
import services.PersonService;
import services.SiteService;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static play.test.Helpers.*;
import static play.test.Helpers.fakeApplication;

/**
 * An integration test focused on testing our routes configuration and interactions with our controller.
 * However we can mock repository interactions here so we don't need a real db.
 */
@RunWith(MockitoJUnitRunner.class)
public class ApplicationIT {

    private static final Long SOME_ID = 1L;
    private static final CharSequence SOME_CONTENT_MESSAGE = "Found id: 1 of person/people";

    private Application app;
    private Global global;

    @Mock
    private PersonService personService;

    @Mock
    private SiteService siteService;

    @Before
    public void setUp() throws Exception {
        app = new Application();
        app.setPersonService(personService);
        app.setSiteService(siteService);

        global = new Global() {
            @Override
            public <A> A getControllerInstance(Class<A> aClass) {
                return (A) app;
            }
        };
    }

    @Test
    public void indexSavesDataAndReturnsId() {
        final Person person = new Person();
        person.setId(SOME_ID);
        when(personService.save(any(Person.class))).thenReturn(person);
        when(personService.findOne(SOME_ID)).thenReturn(person);
        when(siteService.findAll()).thenReturn(Collections.emptyList());

        running(fakeApplication(global),() -> {

            Map<String, String> requestParameters = new HashMap<>();
            requestParameters.put("param1", "Param Value");

            final Result result = route(fakeRequest(GET, "/").withFormUrlEncodedBody(requestParameters));

            assertEquals(OK, status(result));
            assertTrue(contentAsString(result).contains(SOME_CONTENT_MESSAGE));

        });



    }

}
