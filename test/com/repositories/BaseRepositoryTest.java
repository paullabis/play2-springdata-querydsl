package com.repositories;


import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.junit.Before;

/**
 * Created on 5/31/15.
 *
 * @author pLabis
 */
public class BaseRepositoryTest {

    @Before
    public void setUp() throws Exception {
//        IDataSet dataSet = new FlatXmlDataSet()

    }

    protected IDataSet getDataSet() throws Exception {
        FlatXmlDataSetBuilder builder = new FlatXmlDataSetBuilder();
        builder.setDtdMetadata(false);
        return builder.build(getClass().getResourceAsStream("SectionDaoTest.xml") );
    }
}
