import components.play.Global;

import org.dbunit.IDatabaseTester;
import org.dbunit.JdbcDatabaseTester;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.Application;
import play.Configuration;

/**
 * Created on 6/3/15.
 *
 * @author pLabis
 */
public class TestDataGlobal extends Global {

    private static final Logger LOGGER = LoggerFactory.getLogger(TestDataGlobal.class);

    private final String dataFile;

    public TestDataGlobal(String dataFile) {
        this.dataFile = dataFile;
    }

    @Override
    public void onStart(Application application) {
        super.onStart(application);

        try {
            final Configuration configuration = application.configuration();
            final FlatXmlDataSet xmlDataSet = new FlatXmlDataSetBuilder().build(TestDataGlobal.class.getResourceAsStream(dataFile));
            IDatabaseTester dbTester = new JdbcDatabaseTester(configuration.getString("db.test.driver"),
                    configuration.getString("db.test.url"),
                    configuration.getString("db.test.user"),
                    configuration.getString("db.test.password"));
            dbTester.setSetUpOperation(DatabaseOperation.CLEAN_INSERT);
            dbTester.setDataSet(xmlDataSet);
            dbTester.onSetup();
        } catch (Exception e) {
            LOGGER.error("Could not load database", e);
        }
    }

}
