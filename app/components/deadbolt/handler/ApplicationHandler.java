package components.deadbolt.handler;

import be.objectify.deadbolt.core.models.Subject;
import be.objectify.deadbolt.java.AbstractDeadboltHandler;
import be.objectify.deadbolt.java.DynamicResourceHandler;
import play.Logger;
import play.cache.Cache;
import play.i18n.Messages;
import play.libs.F;
import play.mvc.Http;
import play.mvc.Result;

import static org.apache.commons.lang3.StringUtils.isEmpty;

/**
 * Created on 6/3/15.
 *
 * @author pLabis
 */
public class ApplicationHandler extends AbstractDeadboltHandler {

    public static final String SESSION_ID = "SESSION-ID";

    @Override
    public F.Promise<Result> beforeAuthCheck(Http.Context context) {

        String sessionId = context.session().get(SESSION_ID);
        if (isEmpty(sessionId)) {
            context.flash().put("error", Messages.get("should.authenticate"));

            return F.Promise.promise(() -> redirect(controllers.routes.SecurityController.login()));
        }

        return F.Promise.pure(null);
    }

    @Override
    public Subject getSubject(Http.Context context) {
        Subject userSubject = null;
        try {
            String sessionId = context.session().get(SESSION_ID);
            if (isEmpty(sessionId)) {
                return null;
            }

            Object object = Cache.get("USER-" + sessionId);
            if (object == null) {
                return null;
            }

            userSubject = (Subject) object;
        } catch (Throwable throwable) {
            Logger.error("getSubject failed:", throwable);
        }

        return userSubject;
    }

    @Override
    public F.Promise<Result> onAuthFailure(Http.Context context, String content) {
        context.flash().put("error", Messages.get("should.authenticate"));
        return F.Promise.promise(() -> redirect(controllers.routes.SecurityController.login()));
    }

    @Override
    public DynamicResourceHandler getDynamicResourceHandler(Http.Context context) {
        return new ApplicationDynamicResourceHandler();
    }


}
