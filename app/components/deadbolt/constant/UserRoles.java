/*
 * Copyright 2012 Steve Chaloner
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package components.deadbolt.constant;

import be.objectify.deadbolt.core.models.Role;

public enum UserRoles implements Role
{
    ADMIN(Constants.ADMIN),
    QA(Constants.QA),
    CUSTOMER(Constants.CUSTOMER);

    public static class Constants {

        public static final String ADMIN = "admin";
        public static final String QA = "qa";
        public static final String CUSTOMER = "customer";

        private Constants() {}
    }

    private String name;

    UserRoles(String name) {
        this.name = name;
    }

    @Override
    public String getName()
    {
        return name;
    }
}
