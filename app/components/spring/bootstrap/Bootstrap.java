package components.spring.bootstrap;

import components.deadbolt.constant.UserRoles;
import models.AuthorizedUser;
import models.Link;
import models.SecurityRole;
import models.constants.CommonStatus;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import play.Logger;
import repositories.AuthorizedUserRepository;
import repositories.LinkRepository;
import repositories.SecurityRoleRepository;
import repositories.UserPermissionRepository;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created on 6/3/15.
 *
 * @author pLabis
 */
@Service
public class Bootstrap implements InitializingBean {

    @Autowired
    private SecurityRoleRepository securityRoleRepository;

    @Autowired
    private UserPermissionRepository userPermissionRepository;

    @Autowired
    private AuthorizedUserRepository authorizedUserRepository;

    @Autowired
    private LinkRepository linkRepository;

    @Override
    public void afterPropertiesSet() throws Exception {

        if (!securityRoleRepository.findAll().iterator().hasNext()) {
            Arrays.asList(UserRoles.ADMIN,UserRoles.QA,UserRoles.CUSTOMER).forEach(role -> {
                SecurityRole securityRole = new SecurityRole();
                securityRole.setName(role.getName());
                securityRole.setStatus(CommonStatus.ACTIVE.getStatus());
                securityRoleRepository.save(securityRole);

                Logger.info("created security role {}", role.getName());
            });
        }

        if (!authorizedUserRepository.findAll().iterator().hasNext()) {

            AuthorizedUser authorizedUser = new AuthorizedUser();
            authorizedUser.setUsername("admin");
            authorizedUser.setStatus(CommonStatus.ACTIVE.getStatus());
            authorizedUser.setRoles(Arrays.asList(
                    securityRoleRepository.findByName(UserRoles.ADMIN.getName())
            ));
            authorizedUser.setPermissions(new ArrayList<>());
            authorizedUserRepository.save(authorizedUser);
            Logger.info("created user {}", authorizedUser.getIdentifier());

            authorizedUser = new AuthorizedUser();
            authorizedUser.setUsername("qa");
            authorizedUser.setStatus(CommonStatus.ACTIVE.getStatus());
            authorizedUser.setRoles(Arrays.asList(
                    securityRoleRepository.findByName(UserRoles.QA.getName())
            ));
            authorizedUser.setPermissions(new ArrayList<>());
            authorizedUserRepository.save(authorizedUser);
            Logger.info("created user {}", authorizedUser.getIdentifier());
        }

    }
}
