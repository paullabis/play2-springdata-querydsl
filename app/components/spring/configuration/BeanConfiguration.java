package components.spring.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.hibernate3.HibernateExceptionTranslator;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Created on 5/25/15.
 * <p>
 * This configuration establishes Spring Data concerns including those of JPA.
 */
@Configuration
@EnableJpaRepositories("repositories")
@EnableTransactionManagement
public class BeanConfiguration {

    /**
     * The name of the persistence unit we will be using.
     * Replace later with one on configuration.
     */
    static final String DEFAULT_PERSISTENCE_UNIT = "default";

    @Bean
    public EntityManagerFactory entityManagerFactory() {
        return Persistence.createEntityManagerFactory(DEFAULT_PERSISTENCE_UNIT);
    }

    @Bean
    public HibernateExceptionTranslator hibernateExceptionTranslator() {
        return new HibernateExceptionTranslator();
    }

    @Bean
    public JpaTransactionManager transactionManager() {
        return new JpaTransactionManager(entityManagerFactory());
    }

    @Bean
    public be.objectify.deadbolt.java.actions.DynamicAction dynamicAction(){
        return new be.objectify.deadbolt.java.actions.DynamicAction();
    }

    @Bean
    public be.objectify.deadbolt.java.actions.RestrictAction restrictAction(){
        return new be.objectify.deadbolt.java.actions.RestrictAction();
    }

    @Bean
    public play.mvc.Security.AuthenticatedAction securityAuthenticatedAction() {
        return new play.mvc.Security.AuthenticatedAction();
    }

    @Bean
    public be.objectify.deadbolt.java.actions.SubjectPresentAction subjectPresentAction(){
        return new be.objectify.deadbolt.java.actions.SubjectPresentAction();
    }



}
