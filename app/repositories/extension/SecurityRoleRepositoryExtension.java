package repositories.extension;

import models.SecurityRole;

import java.util.List;

/**
 * Created on 5/25/15.
 *
 * @author pLabis
 */
public interface SecurityRoleRepositoryExtension {
    SecurityRole findByName(String name);
}
