package repositories.extension.impl;

import models.*;
import org.springframework.data.jpa.repository.support.QueryDslRepositorySupport;
import repositories.extension.AuthorizedUserRepositoryExtension;

/**
 * Created on 5/25/15.
 *
 * @author pLabis
 */

public class AuthorizedUserRepositoryImpl extends QueryDslRepositorySupport implements AuthorizedUserRepositoryExtension {

    private static final QAuthorizedUser AUTHORIZED_USER = QAuthorizedUser.authorizedUser;

    public AuthorizedUserRepositoryImpl() {
        super(Person.class);
    }

    public AuthorizedUser findByIdentifier(String identifier) {
        return from(AUTHORIZED_USER).where(AUTHORIZED_USER.username.eq(identifier)).singleResult(AUTHORIZED_USER);
    }
}
