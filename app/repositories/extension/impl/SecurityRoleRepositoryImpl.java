package repositories.extension.impl;

import models.Person;
import models.QSecurityRole;
import models.SecurityRole;
import org.springframework.data.jpa.repository.support.QueryDslRepositorySupport;
import repositories.extension.PersonRepositoryExtension;
import repositories.extension.SecurityRoleRepositoryExtension;

import java.util.List;

/**
 * Created on 5/25/15.
 *
 * @author pLabis
 */

public class SecurityRoleRepositoryImpl extends QueryDslRepositorySupport implements SecurityRoleRepositoryExtension {

    private static final QSecurityRole SECURITY_ROLE = QSecurityRole.securityRole;

    public SecurityRoleRepositoryImpl() {
        super(Person.class);
    }

    public SecurityRole findByName(String name) {
        return from(SECURITY_ROLE).where(SECURITY_ROLE.name.eq(name)).singleResult(SECURITY_ROLE);
    }
}
