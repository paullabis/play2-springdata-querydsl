package repositories.extension.impl;

import models.Person;
import models.QPerson;
import org.springframework.data.jpa.repository.support.QueryDslRepositorySupport;

import repositories.extension.PersonRepositoryExtension;

import java.util.List;

/**
 * Created on 5/25/15.
 *
 * @author pLabis
 */

public class PersonRepositoryImpl extends QueryDslRepositorySupport implements PersonRepositoryExtension {

    private static final QPerson PERSON = QPerson.person;

    public PersonRepositoryImpl() {
        super(Person.class);
    }

    /**
     *
     * Sample method using QueryDSL, neat and simple object oriented approach.
     *
     * @return List<Person> persons matching query
     */
    public List<Person> customRepositoryMethod() {
        return from(PERSON).where(PERSON.firstName.contains("c")).list(PERSON);
    }
}
