package repositories.extension;

import models.AuthorizedUser;

/**
 * Created on 5/25/15.
 *
 * @author pLabis
 */
public interface AuthorizedUserRepositoryExtension {
    AuthorizedUser findByIdentifier(String identifier);
}
