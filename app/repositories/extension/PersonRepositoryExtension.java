package repositories.extension;

import java.util.List;

/**
 * Created on 5/25/15.
 *
 * @author pLabis
 */
public interface PersonRepositoryExtension<T> {
    List<T> customRepositoryMethod();
}
