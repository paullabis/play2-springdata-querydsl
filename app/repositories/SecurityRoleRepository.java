package repositories;

import models.SecurityRole;
import models.Site;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import repositories.extension.SecurityRoleRepositoryExtension;

/**
 * Created on 5/26/15.
 *
 * @author pLabis
 */
@Repository
public interface SecurityRoleRepository extends CrudRepository<SecurityRole, Long>, SecurityRoleRepositoryExtension {
}
