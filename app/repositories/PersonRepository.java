package repositories;

import models.Person;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import repositories.extension.PersonRepositoryExtension;

/**
 * Provides CRUD functionality for accessing people. Spring Data auto-magically takes care of many standard
 * operations here.
 */
@Repository
public interface PersonRepository extends CrudRepository<Person, Long>, PersonRepositoryExtension<Person> {

}