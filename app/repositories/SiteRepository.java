package repositories;

import models.Site;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created on 5/26/15.
 *
 * @author pLabis
 */
@Repository
public interface SiteRepository extends CrudRepository<Site, Long> {
}
