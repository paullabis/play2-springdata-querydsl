package repositories;

import models.AuthorizedUser;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import repositories.extension.AuthorizedUserRepositoryExtension;

/**
 * Created on 5/26/15.
 *
 * @author pLabis
 */
@Repository
public interface AuthorizedUserRepository extends CrudRepository<AuthorizedUser, Long>, AuthorizedUserRepositoryExtension {
}
