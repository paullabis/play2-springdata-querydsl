package repositories;

import models.Link;
import models.UserPermission;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created on 5/26/15.
 *
 * @author pLabis
 */
@Repository
public interface LinkRepository extends CrudRepository<Link, Long> {
}
