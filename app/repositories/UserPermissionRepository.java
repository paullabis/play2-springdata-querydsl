package repositories;

import models.SecurityRole;
import models.UserPermission;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created on 5/26/15.
 *
 * @author pLabis
 */
@Repository
public interface UserPermissionRepository extends CrudRepository<UserPermission, Long> {
}
