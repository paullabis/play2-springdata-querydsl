package services;

/**
 * Created on 5/25/15.
 *
 * @author pLabis
 */
public interface GenericService<T> {
    <S extends T> S save(S s);

    <S extends T> java.lang.Iterable<S> save(java.lang.Iterable<S> iterable);

    T findOne(Long id);

    boolean exists(Long id);

    java.lang.Iterable<T> findAll();

    java.lang.Iterable<T> findAll(java.lang.Iterable<Long> iterable);

    long count();

    void delete(Long id);

    void delete(T t);

    void delete(java.lang.Iterable<? extends T> iterable);

    void deleteAll();
}
