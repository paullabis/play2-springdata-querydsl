package services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import play.Logger;
import services.GenericService;

/**
 * Created on 5/25/15.
 *
 * @author pLabis
 */
public class GenericServiceImpl<T, CR extends CrudRepository<T, Long>> implements GenericService<T> {

    @Autowired
    protected CR repository;

    @Autowired
    protected JpaTransactionManager transactionManager;
//    protected PlatformTransactionManager transactionManager;

    public <S extends T> S save(S s) {
        Logger.info("creation from GenericServiceImpl");
        return repository.save(s);
    }

    @Override
    public <S extends T> Iterable<S> save(Iterable<S> iterable) {
        return repository.save(iterable);
    }

    public T findOne(Long id) {
        return repository.findOne(id);
    }

    @Override
    public boolean exists(Long id) {
        return repository.exists(id);
    }

    @Override
    public Iterable<T> findAll() {
        return repository.findAll();
    }

    @Override
    public Iterable<T> findAll(Iterable<Long> iterable) {
        return repository.findAll(iterable);
    }

    @Override
    public long count() {
        return repository.count();
    }

    @Override
    public void delete(Long id) {
        repository.delete(id);
    }

    @Override
    public void delete(T t) {
        repository.delete(t);
    }

    @Override
    public void delete(Iterable<? extends T> iterable) {
        repository.delete(iterable);
    }

    @Override
    public void deleteAll() {
        repository.deleteAll();
    }

    protected TransactionStatus getTransactionStatus() {
        return transactionManager.getTransaction(new DefaultTransactionDefinition());
    }

}