package services.impl;

import models.Site;
import org.springframework.stereotype.Service;
import repositories.SiteRepository;
import services.SiteService;

/**
 * Created on 5/26/15.
 *
 * @author pLabis
 */
@Service
public class SiteServiceImpl extends GenericServiceImpl<Site, SiteRepository> implements SiteService {

}
