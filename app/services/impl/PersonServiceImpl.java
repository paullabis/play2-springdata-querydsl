package services.impl;

import models.Person;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;
import play.Logger;
import repositories.PersonRepository;
import services.PersonService;

import javax.persistence.PersistenceException;
import java.util.Collections;
import java.util.List;

/**
 * Created on 5/25/15.
 *
 * @author pLabis
 */
@Service
public class PersonServiceImpl extends GenericServiceImpl<Person, PersonRepository> implements PersonService {

    @Override
    /**
     * Optionally you can use annotation based transaction handling.
     * @Transactional(propagation = Propagation.REQUIRED, readOnly = true, rollbackFor = RuntimeException.class)
     */
    public List<Person> customServiceMethod() {
        TransactionStatus status = getTransactionStatus();

        try {
            List<Person> persons = repository.customRepositoryMethod();

            persons.forEach(p -> {
                Logger.info("before {}", p.getFirstName());
                p.setFirstName("MyNewName");
                save(p);
            });

            transactionManager.commit(status);
            return persons;
        } catch (PersistenceException ex) {
            Logger.info("do something special when persistence exception occurs {}", ex.getMessage());
            transactionManager.rollback(status);
        } catch (Exception ex) {
            transactionManager.rollback(status);
        }

        return Collections.emptyList();
    }

}
