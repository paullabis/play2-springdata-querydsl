package services.impl;

import models.AuthorizedUser;
import org.springframework.stereotype.Service;
import repositories.AuthorizedUserRepository;
import services.AuthorizedUserService;

/**
 * Created on 5/26/15.
 *
 * @author pLabis
 */
@Service
public class AuthorizedUserServiceImpl extends GenericServiceImpl<AuthorizedUser, AuthorizedUserRepository> implements AuthorizedUserService {

    public AuthorizedUser findByIdentifier(String identifier) {
        return repository.findByIdentifier(identifier);
    }
}
