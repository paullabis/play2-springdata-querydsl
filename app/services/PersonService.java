package services;

import models.Person;

import java.util.List;

/**
 * Created on 5/25/15.
 *
 * @author pLabis
 */
public interface PersonService extends GenericService<Person> {
    List<Person> customServiceMethod();
}
