package services;

import models.Site;

/**
 * Created on 5/25/15.
 *
 * @author pLabis
 */
public interface SiteService extends GenericService<Site> {
}
