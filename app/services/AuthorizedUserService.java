package services;

import models.AuthorizedUser;

/**
 * Created on 5/25/15.
 *
 * @author pLabis
 */
public interface AuthorizedUserService extends GenericService<AuthorizedUser> {
    AuthorizedUser findByIdentifier(String identifier);
}
