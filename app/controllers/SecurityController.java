package controllers;

import be.objectify.deadbolt.java.actions.Dynamic;
import be.objectify.deadbolt.java.actions.Group;
import be.objectify.deadbolt.java.actions.Restrict;
import components.deadbolt.constant.UserRoles;
import models.AuthorizedUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import play.cache.Cache;
import play.mvc.Result;
import services.AuthorizedUserService;

import java.util.UUID;

/**
 * Created on 6/3/15.
 *
 * @author pLabis
 */
@Controller
public class SecurityController extends AbstractController {


    @Autowired
    private AuthorizedUserService authorizedUserService;

    public Result loginNow(String username) {

        AuthorizedUser authorizedUser = authorizedUserService.findByIdentifier(username);

        String sessionId = UUID.randomUUID().toString();

        session().put("SESSION-ID", sessionId);
        Cache.set("USER-" + sessionId, authorizedUser);


//        return ok(views.html.index.render("Found id: " + retrievedPerson.getId() + " of person/people"));
        return ok("Login Form Processor");
    }

    public Result login() {
        return ok("Login Form");
    }

    @Restrict({@Group(UserRoles.Constants.QA), @Group(UserRoles.Constants.ADMIN)})
    public Result onlyQA() {
        return ok("Only QA & Admin");
    }

    @Restrict(@Group(UserRoles.Constants.ADMIN))
    public Result onlyAdmin() {
        return ok("Only ADMIN");
    }

    @Dynamic("allowedLink")
    public Result dynamicRestriction() {
        return ok("Only Authenticated");
    }


}
