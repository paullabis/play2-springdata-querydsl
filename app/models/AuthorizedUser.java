/*
 * Copyright 2012 Steve Chaloner
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package models;

import be.objectify.deadbolt.core.models.Subject;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import java.util.List;


@Entity
public class AuthorizedUser extends BaseModel implements Subject {
    private String username;

    @ManyToMany(fetch = FetchType.EAGER)
    private List<SecurityRole> roles;

    @ManyToMany(fetch = FetchType.EAGER)
    private List<UserPermission> permissions;

    @ManyToMany(fetch = FetchType.EAGER)
    private List<Link> allowedLinks;


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public List<SecurityRole> getRoles() {
        return roles;
    }

    public void setRoles(List<SecurityRole> roles) {
        this.roles = roles;
    }

    public List<UserPermission> getPermissions() {
        return permissions;
    }

    @Override
    public String getIdentifier() {
        return getUsername();
    }

    public void setPermissions(List<UserPermission> permissions) {
        this.permissions = permissions;
    }

    public List<Link> getAllowedLinks() {
        return allowedLinks;
    }

    public void setAllowedLinks(List<Link> allowedLinks) {
        this.allowedLinks = allowedLinks;
    }
}
