package models;

import javax.persistence.Entity;

/**
 * Created on 6/3/15.
 *
 * @author pLabis
 */
@Entity
public class Link extends BaseModel {

    private String uri;
    private String description;

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
