package models;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.List;

/**
 * Created on 5/26/15.
 *
 * @author pLabis
 */
@Entity
public class Site extends BaseModel {

    private String name;

    @OneToMany(mappedBy = "site")
    private List<Person> persons;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Person> getPersons() {
        return persons;
    }

    public void setPersons(List<Person> persons) {
        this.persons = persons;
    }
}
