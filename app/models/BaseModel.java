package models;

import javax.persistence.*;
import java.util.Date;

/**
 * Created on 5/29/15.
 *
 * @author pLabis
 */
@MappedSuperclass
public class BaseModel {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private long id;

    @Column(name = "status")
    private String status;

    @Column(name = "date_created", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable = false, updatable = false)
    private Date dateCreated;

    @Version
    @Column(name = "date_updated")
    private Date dateUpdated;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Date getDateUpdated() {
        return dateUpdated;
    }

    public void setDateUpdated(Date dateUpdated) {
        this.dateUpdated = dateUpdated;
    }
}
