package models.constants;

/**
 * Created on 6/3/15.
 *
 * @author pLabis
 */
public enum CommonStatus {

    ACTIVE("Active"),
    DELETED("Deleted");

    private String status;

    CommonStatus(String status) {
        this.status = status;
    }


    @Override
    public String toString() {
        return getStatus();
    }

    public String getStatus() {
        return status;
    }
}
