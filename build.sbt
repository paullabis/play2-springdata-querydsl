import play.PlayJava

name := "play-spring-querydsl"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava, QueryDSLPlugin)

scalaVersion := "2.11.5"

queryDSLPackage := "models"

libraryDependencies ++= Seq(
    javaCore,
    javaJpa,
    cache,
    "org.springframework" % "spring-context" % "4.0.9.RELEASE",
    "javax.inject" % "javax.inject" % "1",
    "org.springframework.data" % "spring-data-jpa" % "1.8.0.RELEASE",
    "org.springframework" % "spring-expression" % "4.0.9.RELEASE",
    "org.hibernate" % "hibernate-entitymanager" % "4.3.10.Final",
    "org.springframework" % "spring-tx" % "4.0.9.RELEASE",
    "org.mockito" % "mockito-core" % "1.9.5" % "test",
    "org.dbunit" % "dbunit" % "2.5.1" % "test",
    "commons-collections" % "commons-collections" % "3.2.1",
    "be.objectify" % "deadbolt-java_2.11" % "2.3.3"
)

